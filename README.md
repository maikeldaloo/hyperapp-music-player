# Parcel.js and Hyperapp test

This is a quick test to trial the Parcel and Hyperapp.

[Parcel](https://parceljs.org/) is a "Blazing fast, zero configuration web application bundler".

[Hyperapp](https://github.com/hyperapp/hyperapp) is a "1kB JavaScript library for building web applications".

## See it in action

```bash
# Clone the repo.
git clone git@bitbucket.org:maikeldaloo/hyperapp-music-player.git

# Enter the new directory
cd hyperapp-music-player

# Install dependencies.
npm i

# Start up the development server.
npm start
```