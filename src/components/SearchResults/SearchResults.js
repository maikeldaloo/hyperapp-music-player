import { h } from 'hyperapp';
import classnames from 'classnames';

import './SearchResults.css';

export default ({ results, setAlbum, activeTrackId }) => {
	return (
		<div class="search-results">
			<ul>
				{results.map((result) => (
					<li onclick={() => { setAlbum({ album: result }) }}
						className={classnames({ 'is-selected': activeTrackId === result.trackId })}>
						{result.artistName} - {result.trackName}
					</li>
				))}
			</ul>
		</div>
	)
}