import { h } from 'hyperapp';
import classnames from 'classnames';

import Search from '../Search/Search';
import SearchResults from '../SearchResults/SearchResults';
import Player from '../Player/Player';
import Collection from '../Collection/Collection';

import './App.css';

export default (state, actions) => {
	return (
		<div class="app">
			<div class="app__header">
				<Search setTerm={actions.search.SET_TERM} />
			</div>
			<div class="app_body">
				<div class="app__body__results">
					<SearchResults results={state.search.results} setAlbum={actions.album.SET_ALBUM} activeTrackId={state.album.album.trackId} />
				</div>
				<div className={classnames({ 'app__body__player': true, 'hidden': !state.album.album.trackId })}>
					<Player album={state.album.album} />
					<Collection tracks={state.album.related} setAlbum={actions.album.SET_ALBUM} activeTrackId={state.album.album.trackId} />
				</div>
			</div>
		</div>
	)
}