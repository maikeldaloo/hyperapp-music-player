import { h } from 'hyperapp';
import classnames from 'classnames';

import './Collection.css';

export default ({ tracks, setAlbum, activeTrackId }) => {
	return (
		<div class="collection">
			<h5>Songs from the same album</h5>
			
			<ul>
				{tracks.map((track) => (
					<li onclick={() => { setAlbum({ album: track }) }}
						className={classnames({ 'is-selected': activeTrackId === track.trackId })}>
						{track.trackNumber} - {track.trackName}
					</li>
				))}
			</ul>
		</div>
	);
}