import { h } from 'hyperapp';

import './Search.css';

export default ({ setTerm }) => {
	const onSubmit = (event) => {
		event.preventDefault();
		
		const form = event.target || event.srcElement;
		const searchTerm = form.querySelector('.search__term').value;
		
		setTerm({ term: searchTerm });
	}
	
	return (
		<div class="search">
			<form onsubmit={onSubmit} class="search__form">
				<input type="text" name="term" class="search__term" placeholder="Search for an artist" autofocus />
				<button type="submit" class="search__submit">Search</button>
			</form>
		</div>
	);
}