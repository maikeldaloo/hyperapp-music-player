import { h } from "hyperapp";

export default ({ album }) => {
	return (
		<div class="current-song">
			<img src={album.artworkUrl100} alt={album.trackName} />
			<p>{album.artistName} - {album.trackName}</p>
		</div>
	);
}