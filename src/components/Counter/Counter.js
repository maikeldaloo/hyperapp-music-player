import { h } from 'hyperapp'

export default ({ count, minusLater, minus, plus, plusLater }) => {
	return (
		<div>
			<h1>{count}</h1>
			<button onclick={minusLater}>(-)</button>
			<button onclick={minus}>-</button>
			<button onclick={plus}>+</button>
			<button onclick={plusLater}>(+)</button>
		</div>
	)
}
