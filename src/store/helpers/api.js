import axios from 'axios'
import { searchArtistUri, getCollectionUri } from './uri'

/**
 * Sends a request to the API and returns a promise containing the returned results.
 * @param {String} term Artist name
 * @returns {Promise}
 */
export const searchArtist = (term) => {
	return axios.get(searchArtistUri(term))
		.then((response) => {
			if (response.data && response.data.results) {
				return response.data.results || []
			}

			return []
		})
}

/**
 * Sends a request to the API and returns a promise containing songs within the collection/album.
 * @param {Number} collectionId The collection/album ID
 * @returns {Promise}
 */
export const getCollection = (collectionId) => {
	return axios.get(getCollectionUri(collectionId))
		.then((response) => {
			if (response.data && response.data.results) {
				return response.data.results || []
			}

			return []
		})
}

export default {
	searchArtist,
	getCollection
}
