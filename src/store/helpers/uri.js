/**
 * Returns the URI for searching for an artist.
 * @param {String} term artist name
 * @returns {String}
 */
export const searchArtistUri = (term) => {
	return `https://itunes.apple.com/search?term=${term}&limit=25&entity=musicTrack`
}

/**
 * Returns the URI for searching for a collection/album.
 * @param {Number} collectionId The collection/album ID
 * @returns {String}
 */
export const getCollectionUri = (collectionId) => {
	return `https://itunes.apple.com/lookup?id=${collectionId}&entity=song`
}
