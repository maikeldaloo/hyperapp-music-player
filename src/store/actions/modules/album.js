import { getCollection } from '../../helpers/api';

export default {
	SET_ALBUM: (payload) => {
		return (state, actions) => {
			actions.album.FIND_RELATED({ collectionId: payload.album.collectionId });
			
			return payload;
		}
	},
	
	FIND_RELATED: (payload) => {
		return (state, actions) => {
			return getCollection(payload.collectionId).then((results) => {
				payload.related = results.filter((result) => {
					return result.kind === 'song';
				});

				actions.album.SET_RELATED(payload);
			});
		}
	},
	
	SET_RELATED: (payload) => {
		return () => {
			return payload;
		}
	}
}
