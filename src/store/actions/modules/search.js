import { searchArtist } from '../../helpers/api';

export default {
	SET_TERM: (payload) => {
		return (state, actions) => {
			if (!payload.term) {
				payload.results = [];
				
				return payload;
			}
			
			return searchArtist(payload.term).then((results) => {
				payload.results = results;
				
				actions.search.SET_RESULTS(payload);
			});
		}
	},
	
	SET_RESULTS: (payload) => {
		return () => {
			return payload;
		}
	}
}
