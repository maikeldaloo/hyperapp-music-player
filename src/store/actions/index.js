import search from './modules/search'
import album from './modules/album'

export default {
	search,
	album
}