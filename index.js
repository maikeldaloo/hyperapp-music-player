import { app } from 'hyperapp';
import devtools from 'hyperapp-redux-devtools';

import actions from './src/store/actions';
import state from './src/store/state';

import App from './src/components/App/App'

// app(state, actions, App, document.getElementById('app'));
devtools(app)(state, actions, App, document.getElementById('app'));